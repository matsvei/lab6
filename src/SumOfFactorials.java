import java.util.Arrays;

public class SumOfFactorials {
    public static void main(String[] args) {
        int n=5;
        int[] numbers = new int[n];
        initialize(numbers);
        System.out.println(sumFactorial(numbers, n));
    }
    public static int[] initialize(int[] numbers){
        int odd=1;
        for (int i = 0; i< numbers.length; i++){
            numbers[i]=odd;
            odd+=2;
        }
        return numbers;
    }
    public static int factorial(int n)
    {
        int f = 1;
        for (int i = 1; i <= n; i++) {
            f *= i;
        }
        return f;
    }
    static int sumFactorial(int[] numbers, int n)
    {
        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += factorial(numbers[i]);
        }
        return sum;
    }
}
